<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Insure</title>
    <link rel="icon" href="images/favicon-32x32.png" type="image/png" sizes="32*32">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/section.css">
    <link rel="stylesheet" href="css/middle.css">
    <link rel="stylesheet" href="css/float.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/contact.css">
    <link rel="stylesheet" href="css/sign-out.css">
</head>
<style>
    @import url('https://fonts.googleapis.com/css?family=Karla&display=swap');
    @import url('https://fonts.googleapis.com/css?family=DM+Serif+Display&display=swap');
</style>
<body>

<header class="header-class">
    <div class="mobile-header">
        <nav class="insure-icon">
            <a href="index.jsp" class="insure-icon"><img src="images/logo.svg"></a>
        </nav>
        <div class="class-button">
            <input type="image" src="images/icon-hamburger.svg" id="image-menu">
        </div>
    </div>
    <div class="menu-list">
        <a class="menu-list-val" href="#">HOW WE WORK</a>
        <a class="menu-list-val" href="#">BLOG</a>
        <a class="menu-list-val" href="#">ACCOUNT</a>
        <button class="menu-list-button" type="button">VIEW PLANS</button>
        <hr class="hr-header">

        <%

            if ((String) session.getAttribute("email") == null) {

        %>
        <a class="cred-button" href="sign-up.jsp">SIGN UP</a>
        <a class="cred-button" href="sign-in.jsp">SIGN IN</a>
        <%

        } else {

        %>
        <p style="padding-left: 2%"><%=session.getAttribute("user")%>
        </p>
        <form action="auth/signout" method="POST" class="form">
            <input type="submit" value="SIGN OUT" class="signout"/>
        </form>
        <%
            }
        %>
    </div>
    <div id="hamburger-menu">
        <div class="menu-list1">
            <a class="menu-list-val1" href="#">HOW WE WORK</a>
            <a class="menu-list-val1" href="#">BLOG</a>
            <a class="menu-list-val1" href="#">ACCOUNT</a>
            <button class="menu-list-button1" type="button">VIEW PLANS</button>
        </div>
        <div>
            <img src="images/bg-pattern-mobile-nav.svg" id="menu-bg-image">
        </div>
    </div>
</header>