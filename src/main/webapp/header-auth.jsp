<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Insure</title>
    <link rel="icon" href="images/favicon-32x32.png" type="image/png" sizes="32*32">
    <link rel="stylesheet" href="css/sign-up.css">
</head>
<body>

<header>
    <div class="insure-icon">
        <a href="index.jsp" class=""><img src="images/logo.svg"></a>
    </div>
</header>
