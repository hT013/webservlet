package com.mountblue.constant;

public class Constant {

    public static final String OBJECT = "object";
    public static final String E_MAIL = "email";
    public static final String TEMP_E_MAIL = "temp-email";
    public static final String TEMP_UNAME = "temp-uname";
    public static final String USER = "user";
    public static final String PASSWORD = "password";
    public static final String EXIST = "exist";
    public static final String U_NAME = "u-name";
    public static final String CONTACT_FORM = "contactform";
    public static final String INVALID = "invalid";
    public static final String SUCCESS = "Success";
    public static final String FAILED = "Failed";
    public static final String SUCCESS_MESSAGE = "Data saved successfully";
    public static final String FAILED_MESSAGE = "Failed to save data";
    public static final String INVALID_MESSAGE = "Invalid data";

    public static final String EMAIL_PATTERN = "\\w+@\\w+\\.\\w+";
    public static final String PHONE_PATTERN = "[0-9]{10}";
    public static final String EMPTY = "";

    public static final String CONTENT_TYPE = "application/json";
    public static final String UTF_8 = "UTF-8";
    public static final String PERSISTENCE_NAME = "Form";
    public static final String SIGN_IN = "/WebProject/sign-in.jsp";
    public static final String SIGN_UP = "/WebProject/sign-up.jsp";
    public static final String SHOW_DATA = "/WebProject/show-data.jsp";

}